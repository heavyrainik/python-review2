from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, ForeignKey, String

from app import db


class UserXTopic(db.Model):
    __tablename__ = 'user_x_topic'
    author_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    topic_id = Column(Integer, ForeignKey('topic.id'), primary_key=True)

    topic_list = relationship("Topic", back_populates="topic_authors")  # child
    author_list = relationship("User", back_populates="topics")  # parent


class TopicXComment(db.Model):
    comment_id = Column(Integer, ForeignKey('comment.comment_id'), primary_key=True)
    topic_id = Column(Integer, ForeignKey('topic.id'), primary_key=True)

    current_topic = relationship("Topic", back_populates="comments")
    comment_list = relationship("Comment", back_populates="comment_author")


class UserXComment(db.Model):
    comment_id = Column(Integer, ForeignKey('comment.comment_id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)

    c1 = relationship("User", back_populates="user_comments")
    c2 = relationship("Comment", back_populates="c3")


class User(db.Model):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    password = Column(String)

    personal_data = relationship("PersonalData", uselist=False, back_populates="user")
    topics = relationship("UserXTopic", back_populates="author_list")
    user_comments = relationship("UserXComment", back_populates="c1")

    def __init__(self, name, password):
        self.name = name
        self.password = password

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id

    @staticmethod
    def get_name(this_id):
        name = User.query.filter_by(id=this_id).first().name

        return name

    @staticmethod
    def name_is_free(name):
        users = User.query.all()
        is_free = True

        for user in users:
            if user.name == name:
                is_free = False

        return is_free

    @staticmethod
    def password_is_valid(password):
        return password.isalnum()

    @staticmethod
    def check_user(name, password):
        user_name = User.query.filter_by(name=name).first()

        if user_name:
            user_password = User.query.filter_by(name=name).first().password
            if user_password == password:
                return True
            else:
                return False
        else:
            return False


class PersonalData(db.Model):
    user_id = Column(Integer, ForeignKey("user.id"), primary_key=True)
    bio = Column(String(1000))
    country = Column(String(30))
    user = relationship("User", uselist=False, back_populates="personal_data")

    def __init__(self, name=None, bio=None, country=None):
        self.user_id = name
        self.bio = bio
        self.country = country

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.user_id


class Topic(db.Model):
    __tablename__ = 'topic'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(db.String(100))
    content = Column(db.String(2000))
    author_id = Column(db.Integer)

    topic_authors = relationship("UserXTopic", back_populates="topic_list")
    comments = relationship("TopicXComment", back_populates="current_topic")

    def __init__(self, name, content, author_id):
        self.name = name
        self.content = content
        self.author_id = author_id

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.id


class Comment(db.Model):
    __tablename__ = 'comment'
    comment_id = Column(Integer, primary_key=True, autoincrement=True)
    comment = Column(String(250))

    comment_author = relationship("TopicXComment", back_populates="comment_list")
    c3 = relationship("UserXComment", back_populates="c2")

    def __init__(self, comment):
        self.comment = comment
        self.save()

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self.comment_id
