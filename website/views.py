from flask import render_template, redirect, request, session, url_for

from website.forms import RegistrationForm, LoginForm, TopicCreationForm, PasswordChangeForm, BioChangeForm
from app.models import User, Topic, PersonalData, Comment
from app import db
from . import site


@site.route('/')
def index():
    return redirect('/topics')


@site.route('/register', methods=['GET', 'POST'])
def register():
    if session.get('id', '') != '':
        return redirect(url_for('website.user_bio', user_id=session.get('id', '')))

    session['id_client'] = ''

    form = RegistrationForm()

    if form.validate_on_submit():
        free_name = User.name_is_free(form.name.data)
        check_pass = (form.password.data == form.check_password.data)
        password_is_valid = User.password_is_valid(form.password.data)

        if free_name and check_pass and password_is_valid:
            new_user = User(form.name.data, form.password.data)
            session['id'] = User.save(new_user)
            return redirect(url_for('website.user_bio', user_id=session.get('id', '')))
        elif not free_name:
            form.name.errors = ('Название занято', '')
        elif not password_is_valid:
            form.check_password.errors = ('Пароль не может содержать пробелы', '')
        elif not check_pass:
            form.check_password.errors = ('Пароли не совпадают', '')

    return render_template('registration.html', title='Регистрация', form=form)


@site.route('/login', methods=['GET', 'POST'])
def login():
    if session.get('id', '') != '':
        return redirect(url_for('website.user_bio', user_id=session.get('id', '')))

    session['id_client'] = ''

    form = LoginForm()

    if form.validate_on_submit():
        if User.check_user(form.name.data, form.password.data):
            print(User.query.filter_by(name=form.name.data).first().id)
            session['id'] = User.query.filter_by(name=form.name.data).first().id

            return redirect(url_for('website.user_bio', user_id=session.get('id', '')))
        else:
            form.password.errors = ('Неправильная почта или пароль', '')

    return render_template('login.html', title='Вход', form=form)


@site.route('/exit', methods=['GET'])
def exit():
    session['id'] = ''
    return redirect('/')


@site.route('/topics', methods=["GET"])
def display_topics():
    return render_template('topics.html', topics=Topic.query.all(), db=db, User=User, Topic=Topic,
                           title='я даже тут крутой')


@site.route('/create_topic', methods=["GET", "POST"])
def create_topic():
    if session.get('id', '') == '':
        return redirect(url_for('website.login', user_id=session.get('id', '')))

    form = TopicCreationForm()

    if form.validate_on_submit():
        author_id = session.get('id', '')
        new_topic = Topic(form.name.data, form.content.data, author_id)
        Topic.save(new_topic)
        return redirect("/topic/" + str(new_topic.id))

    return render_template('topic_creation.html', title='Новая статья', form=form)


@site.route('/topic/<int:topic_id>', methods=["GET", "POST"])
def topic(topic_id=None):
    if topic_id is None:
        return redirect('/')

    topic = Topic.query.filter_by(id=topic_id).first()

    #topic.comments.append(Comment('fghjk'))

    user = User.query.filter_by(id=topic.author_id).first()
    #comments = topic.comments
    #print(comments)

    return render_template('topic.html', User=user, Topic=topic)


@site.route("/settings", methods=["GET", "POST"])
def change_account():
    user_id = session.get('id', '')

    if user_id == '':
        return redirect("/")

    form1 = PasswordChangeForm()
    form2 = BioChangeForm()

    if form1.validate_on_submit():
        user_password = User.query.filter_by(id=user_id).first().password

        if form1.new_password.data == form1.check_password.data:
            if user_password == form1.old_password.data:
                User.query.filter_by(id=user_id).update({'password': form1.new_password.data})
                db.session.commit()
                return redirect(url_for('website.user_bio', user_id=user_id))
            else:
                form1.old_password.errors = ('Неправильный пароль', '')
        else:
            form1.check_password.errors = ('Пароли не совпадают', '')

    if form2.validate_on_submit():
        data = PersonalData.query.filter_by(user_id=user_id).first()

        if data is not None:
            PersonalData.query.filter_by(user_id=user_id).update({'bio': form2.content.data})
            PersonalData.query.filter_by(user_id=user_id).update({'bio': form2.region.data})
        else:
            user_data = PersonalData(user_id, form2.content.data, form2.region.data)
            PersonalData.save(user_data)

        return redirect(url_for('website.user_bio', user_id=user_id))

    return render_template('account_change.html', title='Изменить настройки', form1=form1, form2=form2)


@site.route('/user/<int:user_id>')
def user_bio(user_id):
    username = User.query.filter_by(id=user_id).first().name
    data = PersonalData.query.filter_by(user_id=user_id).first()

    bio = 'Empty'
    region = 'Empty'

    if data is not None:
        bio = data.bio
        region = data.country

    user_topic = Topic.query.filter_by(author_id=user_id)

    return render_template('user_bio.html', title='Профиль пользователя', username=username, bio=bio, region=region,
                           topics=user_topic)
