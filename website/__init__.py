from flask import Blueprint
from flask import Flask

site = Blueprint('website', __name__, template_folder='templates', static_folder='static')

app = Flask(__name__)

from . import views
