from flask_wtf import FlaskForm
from wtforms import TextAreaField, SelectField, StringField, FieldList, PasswordField, SubmitField, DateField
from wtforms.validators import DataRequired, Length, Email, NumberRange


class RegistrationForm(FlaskForm):
    name = StringField('Название бизнеса',
                       validators=[DataRequired(message="Заполните это поле"),
                                   Length(min=2, max=50,
                                          message='Это поле должно содержать от 2 до 50 символов')])
    password = PasswordField('Пароль',
                             validators=[DataRequired(message="Заполните это поле"),
                                         Length(min=5, max=30,
                                                message='Это поле должно содержать от 5 до 30 символов')])
    check_password = PasswordField('Повторите пароль',
                                   validators=[DataRequired(message="Заполните это поле"),
                                               Length(min=5, max=30,
                                                      message='Это поле должно содержать от 5 до 30 символов')])


class LoginForm(FlaskForm):
    name = StringField('Название бизнеса',
                       validators=[DataRequired(message="Заполните это поле"),
                                   Length(min=2, max=50,
                                          message='Это поле должно содержать от 2 до 50 символов')])
    password = PasswordField('Пароль',
                             validators=[DataRequired(message="Заполните это поле"),
                                         Length(min=5, max=30,
                                                message='Это поле должно содержать от 5 до 30 символов')])


class TopicCreationForm(FlaskForm):
    name = StringField('Название бизнеса',
                       validators=[DataRequired(message="Заполните это поле"),
                                   Length(min=2, max=100,
                                          message='Это поле должно содержать от 2 до 100 символов')])
    content = TextAreaField('Название бизнеса',
                            validators=[DataRequired(message="Заполните это поле"),
                                        Length(min=10, max=5000,
                                               message='Это поле должно содержать от 10 до 5000 символов')])


class PasswordChangeForm(FlaskForm):
    old_password = PasswordField('Старый пароль',
                                 validators=[DataRequired(message="Заполните это поле"),
                                             Length(min=5, max=30,
                                                    message='Это поле должно содержать от 5 до 30 символов')])

    new_password = PasswordField('Новый пароль',
                                 validators=[DataRequired(message="Заполните это поле"),
                                             Length(min=5, max=30,
                                                    message='Это поле должно содержать от 5 до 30 символов')])
    check_password = PasswordField('Повторите пароль',
                                   validators=[DataRequired(message="Заполните это поле"),
                                               Length(min=5, max=30,
                                                      message='Это поле должно содержать от 5 до 30 символов')])


class BioChangeForm(FlaskForm):
    content = TextAreaField('Расскажите о себе',
                            validators=[DataRequired(message="Заполните это поле"),
                                        Length(min=10, max=5000,
                                               message='Это поле должно содержать от 10 до 5000 символов')])
    region = TextAreaField('Укажите вашу страну',
                           validators=[Length(min=0, max=50,
                                              message='Это поле должно содержать от 10 до 5000 символов')])
